## Hive UDFs

* [AbbReplacement](src/main/java/com/example/hive/AbbReplacement.java) : Replaces known abbreviations.
* [CurrentDate](src/main/java/com/example/hive/CurrentDate.java) : Generates current date.
* [CurrentTime](src/main/java/com/example/hive/CurrentTime.java) : Generates current time.
* [DateFormat](src/main/java/com/example/hive/DateFormat.java) : Formats date column in a standard format.
* [Now](src/main/java/com/example/hive/Now.java) : Generates current timestamp.
* [SumArray](src/main/java/com/example/hive/SumArray.java) : Adds all the elements in an Array column.